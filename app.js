var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
Article = require('./models/article');

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

mongoose.connect('mongodb://localhost:27017/nodekb');
var db = mongoose.connection;


// parse application/json
app.use(bodyParser.json())

app.get('/',function(req,res){
	res.send('Use /api/articles');
})

//GET
app.get('/api/articles',function(req,res) {
	
	Article.getArticles(function(err, articles){
		if(err){
			console.log(err);
		}else{
			res.json(articles);
		}
	})
});

//POST
app.post('/api/articles',function(req,res) {
	var article = req.body;
	Article.addArticle(article, function(err, article){
		if(err){
			console.log(err);
		}else{
			res.json(article);
		}
	})
});

//GET

app.get('/api/articles/:_id',function(req,res) {
	
	Article.getArticleById(req.params._id, function(err, article){
		if(err){
			console.log(err);
		}else{
			res.json(article);
		}
	})
});

//UPDATE 
app.put('/api/articles/:_id',function(req,res) {
	var id = req.params._id;
	var article = req.body;
	Article.updateArticle(id, article,{}, function(err, article){
		if(err){
			console.log(err);
		}else{
			res.json(article);
		}
	})
});

//DELETE
app.delete('/api/articles/:_id',function(req,res) {
	var id = req.params._id;
	Article.removeArticle(id, function(err, article){
		if(err){
			console.log(err);
		}else{
			res.json(article);
		}
	})
});

//Start Sever Engine
app.listen(1234,function(){
});